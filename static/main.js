var app = new Vue({
  el: "#qcm",
  data: {
    questions: q,
  },
  methods: {
    displayForm: function (event) {
      const $qs = this.questions;
      const alertPlaceholder = document.getElementById("liveAlertPlaceholder");
      alertPlaceholder.innerHTML =
        '<div class="alert alert-info alert-dismissible" role="alert"><h4 class="alert-heading">Bien joué !</h4><p>Copier les réponses dans un fichier txt nommé avec votre nom et prénom et le mettre dans le dossier \'rep\'.</p><hr>' +
        $qs.map((q) => q.selected) +
        '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
    },
  },
});
